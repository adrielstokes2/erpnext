#!/bin/bash

# Current date and time for filename uniqueness
TIMESTAMP=$(date +%F_%H-%M-%S)

# Backup file name
BACKUP_FILE="db_backup_${TIMESTAMP}.sql"

# Path to save backup file, stored at the top level of frappe_docker
BACKUP_PATH="/home/ubuntu/frappe_docker/${BACKUP_FILE}"

# Path to Dropbox-Uploader script
DROPBOX_UPLOADER="/home/ubuntu/Dropbox-Uploader/dropbox_uploader.sh"

# ERPNext database container name or ID
DB_CONTAINER_NAME="frappe_docker_db_1"  # Ensure this is the correct container name

# Database credentials
DB_NAME="_5e5899d8398b5f7b"  # Use the actual database name
DB_USER="root"
DB_PASSWORD='admin'  # Use the actual database password

# Create a database dump
docker exec $DB_CONTAINER_NAME mysqldump -u $DB_USER -p$DB_PASSWORD $DB_NAME > $BACKUP_PATH

# Upload to Dropbox
$DROPBOX_UPLOADER upload $BACKUP_PATH /

# Uncomment to remove the backup file after upload
# rm $BACKUP_PATH
